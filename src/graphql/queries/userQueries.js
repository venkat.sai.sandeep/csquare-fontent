import gql from 'graphql-tag'

const POST_FRAGMENT = `fragment PostFragment on Post {
  id
  title
  content
}`

export const GET_ALL_POSTS_QUERY = gql`
  query {
    allPosts{
      ...PostFragment
    }
  }
  ${POST_FRAGMENT}
`

export const CREATE_POST_MUTATION = gql`
  mutation createPost($title: String!, $content: String!) {
    createPost(title: $title, content: $content) {
      ...PostFragment
    }
  }
  ${POST_FRAGMENT}
`

export const GET_PAGINATE_POSTS_QUERY = gql`
  query paginatePosts($per_page: Int!, $page: Int!) {
    paginatePosts(per_page: $per_page, page: $page) {
      ...PostFragment
    }
  }
  ${POST_FRAGMENT}
`

export const GET_POST_QUERY = gql`
  query post($id: ID!) {
    post(id: $id) {
      ...PostFragment
    }
  }
  ${POST_FRAGMENT}
`