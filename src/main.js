import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { apolloProvider } from './graphql/apollo'


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  apolloProvider,
  router
}).$mount('#app')
