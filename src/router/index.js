import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'

import constantRoutes from './modules/ContantRoutes.js'

Vue.use(Router)
Vue.use(Meta)

export const contantRoutes = constantRoutes

const createRouter = () => new Router({
  mode: 'history',
  routes: contantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router