var axios = require("axios")

const encode = (str) => {
  return str.replace(/./g, function(c) {
    return ('00' + c.charCodeAt(0)).slice(-3);
  });
}

const modifyUrl = (blog) => {
  const enStr = encode("en-" + blog.id)
  const array = blog.title.split(" ")
  array.push(enStr)
  return array.join("-")
}

const getActiveUsers = () => {
  return axios({
    url: 'https://sample-grapql.herokuapp.com/graphql',
    method: 'post',
    data: {
      query: `
        query posts {
          allPosts {
            id
            title
          }
        }
      `
    }
  }).then((result) => {
    var data = result.data.data.allPosts
    data.map(x => x.id = modifyUrl(x))
    return data
  })
}

module.exports = [
  {
    path: '/',
    component: () => import('@/pages/blogs/index')
  },
  {
    path: '/compose',
    component: () => import('@/pages/blogs/New')
  },
  {
    path: '/about',
    component: () => import('@/pages/about')
  },
  {
    path: '/blog/:id',
    component: () => import('@/pages/blogs/blog'),
    meta: {
      sitemap: {
        slugs: async () => await getActiveUsers(),
        changefreq: 'daily',
      }
    }
  },
  {
    path: '/experiment',
    component: () => import('@/pages/experiment'),
  }
]