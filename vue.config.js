const routes = require('./src/router/modules/ContantRoutes.js')

module.exports = {
  publicPath: '',
  pluginOptions: {
    sitemap: {
        baseURL: 'https://thecsquare.net',
        routes,
    }
  }
}